import React from "react";

import ExpenseDate from "./ExpenseDate";
import Card from "../UI/Card";
import "./ExpenseItem.css";

const ExpenseItem = (expenseData) => {
	/* Return the HTML/JSX code */
	return (
		<li>
			<Card className="expense-item">
				<ExpenseDate date={expenseData.date}></ExpenseDate>
				<div className="expense-item__description">
					<h2>{expenseData.title}</h2>
					<div className="expense-item__price">${expenseData.amount}</div>
				</div>
			</Card>
		</li>
	);
};

export default ExpenseItem;
