import "./ExpenseDate.css";

const ExpenseDate = (expenseDate) => {
    const monthLongVal = expenseDate.date.toLocaleString('en-US', { month: 'long' });
	const dayVal = expenseDate.date.toLocaleString('en-US', { day: '2-digit' });
	const yearVal = expenseDate.date.getFullYear();

	return (
		<div className="expense-date">
			<div className="expense-date__month">{monthLongVal}</div>
			<div className="expense-date__year">{yearVal}</div>
			<div className="expense-date__day">{dayVal}</div>
		</div>
	);
}

export default ExpenseDate;
